﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Boovie.Master" AutoEventWireup="true" CodeBehind="userlogin.aspx.cs" Inherits="BoovieStore.userlogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                    <img width="150" src="https://picsum.photos/150"/>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <center>
                                    <h3>User login</h3>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label>User ID</label>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="TextBox1" runat="server" placeholder="Type in your ID"></asp:TextBox>
                                </div>
                                <label>Password</label>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="TextBox2" runat="server" placeholder="Type in your password" TextMode="Password"></asp:TextBox>
                                </div>
                                <br />
                                <div class="form-group d-grid">
                                    <asp:Button class="btn btn-success btn-lg w-100" ID="Button1" runat="server" Text="Login" />
                                </div>
                                <br />
                                <div class="form-group d-grid">
                                    <a href="usersignup.aspx">
                                        <input class="btn btn-info btn-lg w-100" id="Button2" type="button" value="Sign Up" style="color: whitesmoke;"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="homepage.aspx"><< Back to Homepage</a>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
